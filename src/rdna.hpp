#include <string>
#include <random>

using std::string;

string randDNA(int seed, std::string bases, int n)
{
        int i;
        int min = 0;
        int max = bases.length() -1;

        std::string ATCG = "";

        std::mt19937 eng(seed);
        std::uniform_int_distribution <> uniform(min, max);

        for (i = 0; i < n; i++)
        {
                ATCG += bases[uniform(eng)];
        }

        return ATCG;
}
